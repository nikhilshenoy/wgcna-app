# df_top_genes is the reduced 
get_power <- function(df_top_genes){
  #### Choose Power for scale free topology for cancer 1
  powers = c(c(1:10), seq(from = 12, to = 20, by = 2))
  sft_cancer = pickSoftThreshold(df_top_genes, powerVector = powers, verbose = 5, RsquaredCut = 0.95)
  
  return (sft_cancer$fitIndices$Power[which.max(sft_cancer$fitIndices$SFT.R.sq)])
}