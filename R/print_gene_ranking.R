print_gene_ranking <- function(cancer_top_genes, dynamicMods, MEs_cancer){
  SubGeneNames <- colnames(cancer_top_genes)
  module_colors= setdiff(unique(labels2colors(dynamicMods)), "grey")
  geneModuleMembership = as.data.frame(cor(cancer_top_genes, MEs_cancer, use = "p"));
  
  for (color in module_colors){
    cat (paste("Top 10 genes by Module Membership : ", color, sep = ""))
    cat("\n")
    x = geneModuleMembership[order(geneModuleMembership[, paste("ME", color, sep="")], decreasing= T),]
    cat (rownames(x[1:10,]))
    cat("\n\n")
  }
}